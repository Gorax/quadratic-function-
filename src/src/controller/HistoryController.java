package src.controller;

import src.controller.Service.HistoryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/historyOfResults")
public class HistoryController extends HttpServlet {

    @EJB
    private HistoryService historyService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("history", historyService.getListOfResults());
        req.getRequestDispatcher("/historyOfResults.jsp").forward(req,resp);
    }
}
