package src.controller.Service;

import javax.ejb.Stateful;
import java.util.ArrayList;
import java.util.List;

@Stateful
public class CalculationService {

    public List<Double> calculate(int a, int b, int c) {

        List<Double> scoreList = new ArrayList();

        double delta = b * b - 4 * a * c;
        double x0;
        double x1;
        double x2;

        if (delta < 0) {
            return scoreList;
        } else if (delta == 0) {
            x0 = (-b) / (2 * a);
            scoreList.add(x0);
            return scoreList;
        } else if (delta > 0){
            x1 = (-b - Math.sqrt(delta)) / (2 * a);
            x2 = (-b + Math.sqrt(delta)) / (2 * a);
            scoreList.add(x1);
            scoreList.add(x2);
            return scoreList;
        }
        return null;
    }
}
