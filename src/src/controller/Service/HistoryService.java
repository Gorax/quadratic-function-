package src.controller.Service;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class HistoryService {
    private List<String> listOfResults = new ArrayList<>();

    public List<String> getListOfResults() {
        return listOfResults;
    }
}
