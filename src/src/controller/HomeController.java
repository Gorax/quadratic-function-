package src.controller;

import src.controller.Service.CalculationService;
import src.controller.Service.HistoryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/home")
public class HomeController extends HttpServlet {

    @EJB
    private CalculationService calculationService;

    @EJB
    private HistoryService historyService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int a = Integer.valueOf(req.getParameter("variableA"));
        int b = Integer.valueOf(req.getParameter("variableB"));
        int c = Integer.valueOf(req.getParameter("variableC"));

        if (a == 0) {
            resp.sendRedirect(resp.encodeRedirectURL("home.jsp?invalidData"));
        } else {
            List<Double> scoreList = calculationService.calculate(a, b, c);

            if (scoreList.isEmpty()){
                historyService.getListOfResults().add("Delta < 0");
            }else if (scoreList.size() == 1){
                historyService.getListOfResults().add("x0 = " + scoreList.get(0));
            }else{
                historyService.getListOfResults().add("x1 = " + scoreList.get(0) + " x2 = " + scoreList.get(1));
            }
            req.setAttribute("score", scoreList);
            req.getRequestDispatcher("home.jsp").forward(req, resp);
        }
    }
}
