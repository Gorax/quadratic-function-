<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Item - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-item.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="home.jsp">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="historyOfResults">History of results</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<!-- Page Content -->
<div>

    <h3 style="text-align: center; margin-top: 5%">Please enter numbers</h3>

    <div style="margin-left: 45%; margin-right: 45%; margin-top: 7%;">

        <div style="text-align: left; border-color: #5a6268; border-style: solid; border-radius: 20px; padding: 10px">

            <form method="post" action="home">

                <input style="margin-top: 20px; width: 100%" name="variableA" min="-2_147_483_648" max="2_147_483_647"
                       type="number"
                       placeholder="Enter a" required>

                <input style="margin-top: 20px; width: 100%" name="variableB" min="-2_147_483_648" max="2_147_483_647"
                       type="number"
                       placeholder="Enter b" required>

                <input style="margin-top: 20px; width: 100%" name="variableC" min="-2_147_483_648" max="2_147_483_647"
                       type="number"
                       placeholder="Enter c" required>

                <div style="text-align: center">
                    <input style="border-radius: 15px; margin-top: 20px; width: 100%; text-align: center" type="submit"
                           value="Calculate">
                </div>
            </form>
            <%
                String invalidData = request.getParameter("invalidData");
                if (invalidData != null) {
            %>
            <h6 style="text-align: center; margin-top: 3%">a must be different from 0</h6>
            <%
                }
            %>

            <%
                List<Double> score = (List<Double>) request.getAttribute("score");
                if (score != null) {
                    if (score.isEmpty()) {
            %>
            <h6 style="text-align: center; margin-top: 3%">Delta < 0</h6>
            <%
            } else if (score.size() == 1) {
            %>
            <h6 style="text-align: center; margin-top: 3%"><%="x0 = " + String.format("%.2f", score.get(0))%>
            </h6>
            <%
            } else {
            %>
            <h6 style="text-align: center; margin-top: 3%"><%="x1 = " + String.format("%.2f", score.get(0)) + " x2 = " + String.format("%.2f", score.get(1))%>
            </h6>
            <%
                    }
                }
            %>
        </div>
    </div>
</div>

</body>

</html>
